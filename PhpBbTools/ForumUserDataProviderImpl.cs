﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using Common.Logging;
using PhpBbTools.Db;
using PhpBbTools.Ssh;

namespace PhpBbTools
{
    public class ForumUserDataProviderImpl : IForumUserDataProvider
    {
        private const string SelectSessionId =
            "select session_id from fbguildru_forum1.phpbb_sessions where session_user_id = '{0}'";

        private const string SelectNickName = "select pf_nikname from phpbb_profile_fields_data where user_id = '{0}'";

        private const string SelectGroupType =
            "select g.group_type from phpbb_groups g join phpbb_users u on g.group_id = u.group_id where u.user_id = '{0}'";

        private readonly DbConnectionStringBuilderWrapper _connectionStringBuilder;
        private readonly DbProviderFactory _providerFactory;
        private readonly ISshTunnelFactory _sshTunnelFactory;
        private readonly ILog Logger = LogManager.GetLogger(typeof (ForumUserDataProviderImpl));


        public ForumUserDataProviderImpl(DbConnectionStringBuilderWrapper connectionStringBuilder,
            DbProviderFactory providerFactory,
            ISshTunnelFactory sshTunnelFactory)
        {
            if (connectionStringBuilder == null)
                throw new ArgumentNullException("connectionStringBuilder");
            if (providerFactory == null)
                throw new ArgumentNullException("providerFactory");
            if (sshTunnelFactory == null)
                throw new ArgumentNullException("sshTunnelFactory");

            _sshTunnelFactory = sshTunnelFactory;
            _providerFactory = providerFactory;
            _connectionStringBuilder = connectionStringBuilder;
        }

        public IEnumerable<string> GetUserSessions(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentNullException("userId");

            var tunnel = _sshTunnelFactory.GetSshTunnel();
            return ExecuteCommandInConnection(command =>
            {
                using (IDataReader reader = command.ExecuteReader())
                {
                    var tb = new DataTable();
                    tb.Load(reader);
                    var results = tb.Rows.Cast<DataRow>().Select(row => (string) row.ItemArray[0]).ToList();
                    return results;
                }
            }, tunnel, string.Format(SelectSessionId, userId));
        }

        public string GetUserCharacter(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentNullException("userId");

            var tunnel = _sshTunnelFactory.GetSshTunnel();
            return ExecuteCommandInConnection((command => (string)command.ExecuteScalar()), tunnel,
                string.Format(SelectNickName, userId));
        }

        public int GetUserGroupType(string userId)
        {
            if (string.IsNullOrEmpty(userId))
                throw new ArgumentNullException("userId");

            var tunnel = _sshTunnelFactory.GetSshTunnel();
            return ExecuteCommandInConnection((command => (int) command.ExecuteScalar()), tunnel,
                string.Format(SelectGroupType, userId));
        }

        private T ExecuteCommandInConnection<T>(Func<DbCommand, T> func, SshTunnel tunnel, string commandText)
        {
            Logger.Debug(string.Format("Connection get for {0}", commandText));
            using (var connection = _providerFactory.CreateConnection())
            {
                _connectionStringBuilder.SetPort(checked((uint) tunnel.LocalPort));

                Debug.Assert(connection != null, "connection != null");
                connection.ConnectionString = _connectionStringBuilder.ConnectionString;
                using (var com = connection.CreateCommand())
                {
                    com.CommandText = commandText;
                    connection.Open();
                    Logger.Debug(string.Format("Connection is open for {0}", commandText));
                    return func(com);
                }
            }
        }

    }
}