﻿namespace PhpBbTools
{
    /// <summary>
    /// Login operation results
    /// </summary>
    public class ForumLoginResult
    {
        /// <summary>
        /// User id of login operation, empty if login was not successful
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Status of login operation
        /// </summary>
        public LoginCallStatus Status { get; set; }
        /// <summary>
        /// Id of session in which user logged in, empty if login was not successful
        /// </summary>
        public string SessionId { get; set; }
    }
}
