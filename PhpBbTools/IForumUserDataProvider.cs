﻿using System.Collections.Generic;

namespace PhpBbTools
{
    /// <summary>
    /// Client for phpbb user base
    /// </summary>
    public interface IForumUserDataProvider
    {
        /// <summary>
        /// gets all session ids for => user id
        /// </summary>
        /// <param name="userId">id of user</param>
        /// <returns>list of session ids</returns>
        IEnumerable<string> GetUserSessions(string userId);
        /// <summary>
        /// character nickname
        /// </summary>
        /// <param name="userId">id of user</param>
        /// <returns>user character nickname</returns>
        string GetUserCharacter(string userId);
        /// <summary>
        /// gets user current group type id
        /// </summary>
        /// <param name="userId">id of user</param>
        /// <returns>id of group type for user</returns>
        int GetUserGroupType(string userId);
    }
}
