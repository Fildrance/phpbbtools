﻿using System.Data.Common;
using Common.Logging;

namespace PhpBbTools.Db
{
    public class DbConnectionStringBuilderWrapper
    {
        private static readonly object Lock = new object();
        private readonly DbConnectionStringBuilder _connectionStringBuilder;
        private readonly ILog Logger = LogManager.GetLogger(typeof (DbConnectionStringBuilderWrapper));

        public DbConnectionStringBuilderWrapper(DbConnectionStringBuilder connectionStringBuilder)
        {
            _connectionStringBuilder = connectionStringBuilder;
        }

        public virtual string ConnectionString
        {
            get { return _connectionStringBuilder.ConnectionString; }
        }

        public virtual void SetPort(uint port)
        {
            if (ContainsEqualPort(port))
                lock (Lock)
                {
                    if (ContainsEqualPort(port))
                    {
                        Logger.Debug(string.Format("Setting port {0} for db connection", port));
                        _connectionStringBuilder.Add("Port", port);
                    }
                }
        }

        private bool ContainsEqualPort(uint port)
        {
            return !_connectionStringBuilder.ContainsKey("Port") || (uint) _connectionStringBuilder["Port"] != port;
        }
    }
}