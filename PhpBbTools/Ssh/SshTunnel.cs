﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using Common.Logging;
using Renci.SshNet;

namespace PhpBbTools.Ssh
{
    public class SshTunnel
    {
        private readonly SshClient _client;
        private int _localPort;
        private readonly ForwardedPortLocal _port;
        private readonly ILog Logger = LogManager.GetLogger(typeof (SshTunnel));

        internal SshTunnel() { }

        /// <summary>
        ///     Creates and activates ssh tunnel
        /// </summary>
        /// <param name="sshHost">host url for ssh connection</param>
        /// <param name="sshUser">user name for ssh connection</param>
        /// <param name="sshPassword">password for ssh connection</param>
        /// <param name="remotePort">port for ssh connection to be used during tunnel active time</param>
        public SshTunnel(string sshHost, string sshUser, string sshPassword, uint remotePort)
        {
            var connectionInfo = new ConnectionInfo(sshHost, sshUser,
                new PasswordAuthenticationMethod(sshUser, sshPassword)) 
                {Timeout = TimeSpan.FromMinutes(1)};
            _client = new SshClient(connectionInfo);
            _port = new ForwardedPortLocal("127.0.0.1", 0, "127.0.0.1", remotePort);
            EstablishConnection();
        }

        //enables to get local port of connection that can be used for ssh interaction with remote machine during tunnel active time
        public virtual int LocalPort
        {
            get { return _localPort; }
        }

        public bool IsConnected
        {
            get { return _client.IsConnected; }
        }

        public void Connect()
        {
            if(_client.IsConnected)
                throw new InvalidOperationException("Connection is already established - you cannot ");
            EstablishConnection();
        }

        internal void Disconnect()
        {
            Logger.Debug("Disconnecting tunnel");
            _port.Stop();
            _client.Disconnect();
        }

        private void EstablishConnection()
        {
            _client.Connect();
            Logger.Debug("Connection established");
            if(!_client.ForwardedPorts.Any())
                _client.AddForwardedPort(_port);
            if(!_port.IsStarted)
                _port.Start();
            Logger.Debug("Port forwarding started");

            // Hack to allow dynamic local ports, ForwardedPortLocal should expose _listener.LocalEndpoint
            var listener =
                (TcpListener)
                    typeof(ForwardedPortLocal).GetField("_listener",
                        BindingFlags.Instance | BindingFlags.NonPublic).GetValue(_port);
            _localPort = ((IPEndPoint)listener.LocalEndpoint).Port;
            Logger.Debug("Local port for connection tunnel is " + _localPort);
        }

        private void Dispose()
        {
            Logger.Debug("Disposing of tunnel");
            if (_client.IsConnected)
                _client.Disconnect();
            if (_port != null)
                _port.Dispose();
            if (_client != null)
                _client.Dispose();
        }

        ~SshTunnel()
        {
            Dispose();
        }
    }
}