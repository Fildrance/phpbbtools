﻿using System;
using System.Threading;
using Common.Logging;

namespace PhpBbTools.Ssh
{
    public class SshTunnelFactory : ISshTunnelFactory
    {
        private readonly ILog Logger = LogManager.GetLogger(typeof (SshTunnelFactory));

        #region c-tor

        public SshTunnelFactory(string sshHost, string sshUser, string sshPassword, uint port,
            TimeSpan? tunnelLifetimeSpan = null)
        {
            Logger.Debug("Initializing ssh tunnel factory with host=" + sshHost + " user=" + sshUser + " port=" + port);

            _sshHost = sshHost;
            _sshUser = sshUser;
            _sshPassword = sshPassword;
            _port = port;

            if (tunnelLifetimeSpan.HasValue)
            {
                SshTunnelLifetimeSpan = tunnelLifetimeSpan.Value;
            }
        }

        #endregion

        #region public methods

        public SshTunnel GetSshTunnel()
        {
            if (_tunnel == null)
            {
                lock (_lock)
                {
                    if (_tunnel == null)
                    {
                        Logger.Info("Initializing tunnel");
                        _tunnel = new SshTunnel(_sshHost, _sshUser, _sshPassword, _port);
                    }
                }
            }
            if (!_tunnel.IsConnected)
            {
                Logger.Info("Reconnecting tunnel");
                _tunnel.Connect();
            }
            StartTimerIfNotActive();
            return _tunnel;
        }

        public TimeSpan TunnelLifetimeSpan
        {
            set { SshTunnelLifetimeSpan = value; }
        }

        #endregion

        #region fields

        private readonly TimeSpan NoRepeatTimeSpan = new TimeSpan(0, 0, 0, 0, -1);
        private TimeSpan SshTunnelLifetimeSpan = TimeSpan.FromMinutes(3);

        private readonly object _lock = new object();
        private readonly uint _port;
        private readonly string _sshHost;
        private readonly string _sshPassword;
        private readonly string _sshUser;


        private Timer _timer;
        private SshTunnel _tunnel;

        #endregion

        #region private methods

        private void StartTimerIfNotActive()
        {
            if (_timer == null)
            {
                lock (_lock)
                {
                    if (_timer == null)
                    {
                        Logger.Debug("Starting new timer for ssh tunnel lifetime for " + SshTunnelLifetimeSpan);
                        _timer = new Timer(checkStatus, null, SshTunnelLifetimeSpan, NoRepeatTimeSpan);
                    }
                }
            }
            else
            {
                Logger.Debug("Prolonging ssh tunnel lifetime for another "+ SshTunnelLifetimeSpan);
                _timer.Change(SshTunnelLifetimeSpan, NoRepeatTimeSpan);
            }
        }

        private void checkStatus(object obj)
        {
            Logger.Debug("Ssh tunnel lifetime has run out - now it's time to shut it down");
            _tunnel.Disconnect();
            _timer = null;
        }

        #endregion
    }
}