﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhpBbTools.Ssh
{
    /// <summary>
    /// Factory for creating ssh tunnel
    /// </summary>
    public interface ISshTunnelFactory
    {
        /// <summary>
        /// Creates Ssh tunnel ready to use. Is disconnected after timeout 
        /// so better to use it untile timeout will ran out.
        /// If for any activiti tunnel will be acquired once more, timer will be prolonged.
        /// </summary>
        /// <returns>currently active ssh tunnel object</returns>
        SshTunnel GetSshTunnel();
        /// <summary>
        /// Timespan in which tunnel 
        /// </summary>
        TimeSpan TunnelLifetimeSpan { set; }
    }
}
