﻿using System.Threading.Tasks;

namespace PhpBbTools
{
    /// <summary>
    /// Client for forum web api
    /// </summary>
    public interface IForumClient
    {
        /// <summary>
        /// Login into bbh forum and auto logout if logout is enabled
        /// </summary>
        /// <param name="login">user login</param>
        /// <param name="password">user password</param>
        /// <returns>returns task, encapsulating the login operation</returns>
        Task<ForumLoginResult> Login(string login, string password);
    }
}