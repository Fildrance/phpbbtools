﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Common.Logging;

namespace PhpBbTools
{
    public class ForumClientImpl : IForumClient
    {
        #region c-tor

        public ForumClientImpl(string baseUrl, string loginPath, string logoutPath = null)
        {
            if (string.IsNullOrEmpty(baseUrl))
                throw new ArgumentNullException("baseUrl");
            if (string.IsNullOrEmpty(loginPath))
                throw new ArgumentNullException("loginPath");

            Logger.Debug(string.Format("Initializing forum client with base url={0} loginPath={1} logoutPath={2}",
                baseUrl, loginPath, logoutPath));
            _baseUri = new Uri(baseUrl);
            _loginUri = new Uri(baseUrl + loginPath);
            if (!string.IsNullOrEmpty(logoutPath))
            {
                _logoutUri = new Uri(baseUrl + logoutPath);
                _autoLogOut = true;
            }
        }

        #endregion

        public async Task<ForumLoginResult> Login(string login, string password)
        {
            if (string.IsNullOrEmpty(login))
                throw new ArgumentNullException("login");
            if (string.IsNullOrEmpty(password))
                throw new ArgumentNullException("password");

            Logger.Debug(string.Format("Begin login try for login={0}", login));
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler {CookieContainer = cookieContainer})
            {
                var client = new HttpClient(handler);
                SetupHeaders(client.DefaultRequestHeaders);
                var content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", login),
                    new KeyValuePair<string, string>("password", password),
                    new KeyValuePair<string, string>("autologin", "on"),
                    new KeyValuePair<string, string>("redirect", "o.%2Fucp.php%3Fmode%3Dloginn"),
                    new KeyValuePair<string, string>("redirect", "index.php"),
                    new KeyValuePair<string, string>("login", "%D0%92%D1%85%D0%BE%D0%B4")
                });

                var resp = await client.PostAsync(_loginUri, content);
                var uid = GetUserIdFromCookie(cookieContainer);
                var sid = GetSidIfUserLoggedIn(cookieContainer);
                if (!resp.IsSuccessStatusCode || uid <= 1 || string.IsNullOrEmpty(sid))
                {
                    Logger.Warn(string.Format(
                        "Login for login = {0} failed. Response state is {1} and uid is {2}" + uid, login,
                        resp.StatusCode, uid));
                    return new ForumLoginResult {Status = LoginCallStatus.LoginError, UserId = null, SessionId = sid};
                }
                if (_autoLogOut)
                {
                    Logger.Debug(string.Format("Logout start for login={0}", login));
                    var logoutResult = await LogOut(client, cookieContainer, sid);
                    return new ForumLoginResult {Status = logoutResult, UserId = sid, SessionId = sid};
                }
                return new ForumLoginResult {Status = LoginCallStatus.Ok, UserId = sid, SessionId = sid};
            }
        }

        private async Task<LoginCallStatus> LogOut(HttpClient client, CookieContainer cookieContainer, string sid)
        {
            var resp = await client.GetAsync(_logoutUri + "&sid=" + sid);

            if (resp.IsSuccessStatusCode && GetUserIdFromCookie(cookieContainer) == 1)
            {
                return LoginCallStatus.Ok;
            }
            return LoginCallStatus.LogoutError;
        }


        private string GetSidIfUserLoggedIn(CookieContainer cookieContainer)
        {
            string sid = null;
            var cookie = cookieContainer.GetCookies(_baseUri)["phpbb3_96qxl_sid"];
            if (cookie != null)
                sid = cookie.Value;
            return sid;
        }

        private long? GetUserIdFromCookie(CookieContainer cookieContainer)
        {
            var cookie = cookieContainer.GetCookies(_baseUri)["phpbb3_96qxl_u"];
            long cookieValue;
            if (cookie != null
                && long.TryParse(cookie.Value, out cookieValue))
                return cookieValue;
            return null;
        }

        private void SetupHeaders(HttpRequestHeaders headers)
        {
            headers.CacheControl = new CacheControlHeaderValue {MaxAge = new TimeSpan(0)};
            headers.Add("Connection", "keep-alive");
            headers.Add("Host", "forum.fb-guild.ru");
            headers.Add("Referer", _loginUri.AbsoluteUri);
            headers.Add("Origin", _baseUri.AbsoluteUri);
            headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
            headers.Add("Accept-Encoding", "gzip, deflate");
            headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            headers.Add("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");
            headers.Add("Upgrade-Insecure-Requests", "1");
        }

        #region Fields

        private readonly ILog Logger = LogManager.GetLogger(typeof (ForumClientImpl));
        private readonly bool _autoLogOut;
        private readonly Uri _baseUri;
        private readonly Uri _loginUri;
        private readonly Uri _logoutUri;

        #endregion
    }

    public enum LoginCallStatus
    {
        Ok,
        LoginError,
        LogoutError
    }
}