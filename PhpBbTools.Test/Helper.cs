﻿using System;
using System.Data.Common;
using System.Diagnostics;
using MySql.Data.MySqlClient;
using PhpBbTools.Db;
using PhpBbTools.Ssh;

namespace PhpBbTools.Test
{
    static class Helper
    {
        public static IForumUserDataProvider CreateProvider(TimeSpan? tunnelLifeSpan = null)
        {
            MySqlClientFactory provider = MySqlClientFactory.Instance;
            DbConnectionStringBuilder connectionStringBuilder = provider.CreateConnectionStringBuilder();
            Debug.Assert(connectionStringBuilder != null, "connectionStringBuilder != null");
            connectionStringBuilder.Add("database", "fbguildru_forum1");
            connectionStringBuilder.Add("userId", "fbguildru_1");
            connectionStringBuilder.Add("password", "Df2hr0hf1a3N");
            connectionStringBuilder.Add("server", "127.0.0.1");
            connectionStringBuilder.Add("Pooling", "True");
            connectionStringBuilder.Add("maximumpoolsize", 8);
            return new ForumUserDataProviderImpl(new DbConnectionStringBuilderWrapper(connectionStringBuilder), provider,
                CreateTunnelFactory(tunnelLifeSpan));
        }

        public static ISshTunnelFactory CreateTunnelFactory(TimeSpan? tunnelLifeSpan = null)
        {
            return new SshTunnelFactory("fb-guild.ru", "fbguildru", "Gh57skfj6gHfn", 3306, tunnelLifeSpan);
        }
    }
}
