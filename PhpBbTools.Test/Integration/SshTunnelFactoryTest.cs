﻿using System;
using System.Threading;
using Common.Logging;
using NUnit.Framework;
using PhpBbTools.Ssh;

namespace PhpBbTools.Test.Integration
{
    [TestFixture]
    public class SshTunnelFactoryTest : TestSupportBase
    {
        
        private ISshTunnelFactory _factory;

        [Test]
        public void DefaultLifetimeTest()
        {
            ILog Logger = LogManager.GetLogger(typeof(SshTunnelFactoryTest));
            //arrange
            _factory = Helper.CreateTunnelFactory();
            //act
            var tunnel = _factory.GetSshTunnel();
            Logger.Debug("tunnel connected is " + tunnel.IsConnected);
            Thread.Sleep(TimeSpan.FromMinutes(1));
            Logger.Debug("tunnel connected is " + tunnel.IsConnected);
            Thread.Sleep(TimeSpan.FromMinutes(1));
            Logger.Debug("tunnel connected is " + tunnel.IsConnected);
            Thread.Sleep(new TimeSpan(0,0,1,15));
            //assert
            Logger.Debug("tunnel connected is " + tunnel.IsConnected);
            Assert.False(tunnel.IsConnected);
        }

        [Test]
        public void LifetimeTimerTest()
        {
            //arrange
            _factory = Helper.CreateTunnelFactory(TimeSpan.FromSeconds(5));
            //act
            var tunnel = _factory.GetSshTunnel();
            Assert.True(tunnel.IsConnected);
            Thread.Sleep(TimeSpan.FromSeconds(1));
            Assert.True(tunnel.IsConnected);
            Thread.Sleep(TimeSpan.FromSeconds(2));
            Assert.True(tunnel.IsConnected);
            Thread.Sleep(TimeSpan.FromSeconds(3));
            //assert
            Assert.False(tunnel.IsConnected);
        }
    }
}