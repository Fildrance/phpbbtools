﻿using System.Linq;
using System.Net.Http;
using NUnit.Framework;

namespace PhpBbTools.Test.Integration
{
    [TestFixture]
    [Category("PhpBbTools.Test.Integration")]
    public class ForumIntegrationTest : TestSupportBase
    {
        [SetUp]
        public void Setup()
        {
            _client = new ForumClientImpl(BaseUrl, LoginPath, LogoutPath);
        }

        private IForumClient _client;
        private const string BaseUrl = "http://forum.fb-guild.ru";
        private const string LoginPath = "/ucp.php?mode=login";
        private const string LogoutPath = "/ucp.php?mode=logout";



        [Test]
        public void LoginTest()
        {
            //arrange

            //act
            var result = _client.Login("Fildrance", "MP3NWFBNGE33").Result;
            //assert
            Assert.AreEqual(LoginCallStatus.Ok, result.Status);
            var httpClient = new HttpClient();
            HttpResponseMessage forumSiteResp = httpClient.GetAsync("http://forum.fb-guild.ru").Result;
            //check that site is not blocking us
            Assert.True(forumSiteResp.IsSuccessStatusCode);
            //check that we havve no session
            var sessionIds = Helper.CreateProvider().GetUserSessions("2919");
            Assert.False(sessionIds.Any(x=>x== result.SessionId));
        }

        [Test]
        public void LoginTest_noAutoLogOut()
        {
            //arrange
            _client = new ForumClientImpl(BaseUrl, LoginPath);
            //act
            var result = _client.Login("Fildrance", "MP3NWFBNGE33").Result;
            //assert
            Assert.AreEqual(LoginCallStatus.Ok, result.Status);
            var httpClient = new HttpClient();
            HttpResponseMessage forumSiteResp = httpClient.GetAsync("http://forum.fb-guild.ru").Result;
            //check that site is not blocking us
            Assert.True(forumSiteResp.IsSuccessStatusCode);
            var sessionsIds = Helper.CreateProvider().GetUserSessions("2919");
            Assert.True(sessionsIds.Any(x=>x==result.SessionId));
        }

        [Test]
        public void GetUserSessionTest()
        {
            //arrange
            IForumUserDataProvider provider = Helper.CreateProvider();
            //act
            var character = provider.GetUserCharacter("2919");
            //assert
            Assert.AreEqual("Филдранс",character);
        }
    }
}