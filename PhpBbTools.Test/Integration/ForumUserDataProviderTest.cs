﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace PhpBbTools.Test.Integration
{
    [TestFixture]
    public class ForumUserDataProviderTest : TestSupportBase
    {
        private const string expectedCharacterName = "Филдранс";

        [SetUp]
        public void Setup()
        {
            _provider = Helper.CreateProvider();
        }

        private IForumUserDataProvider _provider;

        [Test]
        public void ConcurrencyTest()
        {
            var tasks = new List<Task<string>>();
            for (var i = 0; i < 50; i++)
            {
                var task = new Task<string>(() => _provider.GetUserCharacter("2919"));
                tasks.Add(task);
            }
            tasks.ForEach(x=>x.Start());

            tasks.WaitAll();
            tasks.ForEach(x =>
            {
                Assert.AreEqual(expectedCharacterName, x.Result);
            });
        }

        [Test]
        public void ReconnectTest()
        {
            //arrange
            _provider = Helper.CreateProvider(TimeSpan.FromSeconds(8));
            //act
            var characterName = _provider.GetUserCharacter("2919");
            Assert.AreEqual(expectedCharacterName, characterName);
            Thread.Sleep(TimeSpan.FromSeconds(10));
            //assert
            characterName = _provider.GetUserCharacter("2919");
            Assert.AreEqual(expectedCharacterName, characterName);
        }
    }


    static class TaskExtensions
    {
        public static void WaitAll<T>(this IEnumerable<Task<T>> tasks)
        {
            foreach (var item in tasks)
            {
                item.Wait();
            }
        }
    }
}