﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.Logging;
using Common.Logging.Simple;
using NUnit.Framework;

namespace PhpBbTools.Test
{
    public abstract class TestSupportBase
    {
        [TestFixtureSetUp]
        public void FixtureSetup()
        {
            LogManager.Adapter = new ConsoleOutLoggerFactoryAdapter();
        }
    }
}
