﻿using System;
using NUnit.Framework;

namespace PhpBbTools.Test.Unit
{
    [TestFixture]
    [Category("PhpBbTools.Test.Unit")]
    public class ForumClientTest : TestSupportBase
    {
        [SetUp]
        public void Setup()
        {
            _client = new ForumClientImpl("http://basicUrl/", "loginPath");
        }

        private IForumClient _client;

        [Test]
        public void ConstructorTest_nobaseUrl()
        {
            //arrange
            //act
            Assert.Throws<ArgumentNullException>(() => new ForumClientImpl(null, "somePath"));
            //assert
        }

        [Test]
        public void ConstructorTest_noLoginUrl()
        {
            //arrange
            //act
            Assert.Throws<ArgumentNullException>(() => new ForumClientImpl("basicUrl", null));
            //assert
        }

        [Test]
        public void LoginTest_LoginIsNull()
        {
            Assert.Throws<ArgumentNullException>(()=>
            {
                try
                {
                    var forumLoginResult = _client.Login(null, "myPassword").Result;
                }
                catch (AggregateException ex)
                {
                    throw ex.InnerException;
                }
            });
        }

        [Test]
        public void LoginTest_PasswordIsNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                try
                {
                    var forumLoginResult = _client.Login("myLogin", null).Result;
                }
                catch (AggregateException ex)
                {
                    throw ex.InnerException;
                }
            });
        }
    }
}