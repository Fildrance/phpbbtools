﻿using System;
using System.Data;
using System.Data.Common;
using NUnit.Framework;
using PhpBbTools.Db;
using PhpBbTools.Ssh;
using Rhino.Mocks;

namespace PhpBbTools.Test.Unit
{
    [TestFixture]
    [Category("PhpBbTools.Test.Unit")]
    public class ForumUserDataProviderTest : TestSupportBase
    {
        [SetUp]
        public void Setup()
        {
            _mockRepository = new MockRepository();
            _builder = _mockRepository.Stub<DbConnectionStringBuilderWrapper>(_mockRepository.Stub<DbConnectionStringBuilder>());
            _factory = _mockRepository.Stub<DbProviderFactory>();
            _sshTunnelFactory = _mockRepository.Stub<ISshTunnelFactory>();
            _provider = new ForumUserDataProviderImpl(_builder, _factory, _sshTunnelFactory);
        }

        private MockRepository _mockRepository;
        private IForumUserDataProvider _provider;
        private ISshTunnelFactory _sshTunnelFactory;
        private DbProviderFactory _factory;
        private DbConnectionStringBuilderWrapper _builder;
        private const int PORT = 55;

        private const string EXPECTED_ID = "actualId";
        private const string USER_ID = "userId";

        private const string CONNECTION_STRING =
            @"server=myServerAddress;database=myDataBase;user id=myUsername;password=myPassword;";

        public void GetUserSessionsTest_userIdIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => _provider.GetUserSessions(null));
        }

        [Test]
        public void ConstructorTest_ConnectionBuilderIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new ForumUserDataProviderImpl(null, _factory, _sshTunnelFactory));
        }

        [Test]
        public void ConstructorTest_DbProviderIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new ForumUserDataProviderImpl(_builder, null, _sshTunnelFactory));
        }

        [Test]
        public void ConstructorTest_SshTunnelFactoryIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => new ForumUserDataProviderImpl(_builder, _factory, null));
        }

        [Test]
        public void GetUserCharacterTest()
        {
            //arrange
            SshTunnel tunel = new SshTunnel();
            var tunnel = _mockRepository.Stub<SshTunnel>();
            var command = _mockRepository.Stub<DbCommand>();
            var connection = _mockRepository.PartialMock<DbConnection>();

            using (_mockRepository.Record())
            {
                Expect.Call(_sshTunnelFactory.GetSshTunnel()).Return(tunnel);
                Expect.Call(_factory.CreateConnection()).Return(connection);
                Expect.Call(_builder.ConnectionString).Return(CONNECTION_STRING);
                Expect.Call(tunnel.LocalPort).Return(PORT);
                Expect.Call(connection.ConnectionString).SetPropertyWithArgument(CONNECTION_STRING);
                Expect.Call(connection.CreateCommand()).Return(command);
                Expect.Call(connection.Open);
                Expect.Call(command.ExecuteScalar()).Return(EXPECTED_ID);
            }
            //act
            var actual = _provider.GetUserCharacter(USER_ID);
            //assert
            Assert.AreEqual(EXPECTED_ID, actual);
            Assert.AreEqual("select pf_nikname from phpbb_profile_fields_data where user_id = '" + USER_ID + "'",
                command.CommandText);
        }

        [Test]
        public void GetUserCharacterTest_userIdIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => _provider.GetUserCharacter(null));
        }

        [Test]
        public void GetUserGroupTypeTest()
        {
            //arrange
            var tunnel = _mockRepository.Stub<SshTunnel>();
            var command = _mockRepository.Stub<DbCommand>();
            var connection = _mockRepository.PartialMock<DbConnection>();

            using (_mockRepository.Record())
            {
                Expect.Call(_sshTunnelFactory.GetSshTunnel()).Return(tunnel);
                Expect.Call(_factory.CreateConnection()).Return(connection);
                Expect.Call(_builder.ConnectionString).Return(CONNECTION_STRING);
                Expect.Call(tunnel.LocalPort).Return(PORT);
                Expect.Call(connection.ConnectionString).SetPropertyWithArgument(CONNECTION_STRING );
                Expect.Call(connection.CreateCommand()).Return(command);
                Expect.Call(connection.Open);
                Expect.Call(command.ExecuteScalar()).Return(66);
            }
            //act
            var actual = _provider.GetUserGroupType(USER_ID);
            //assert
            Assert.AreEqual(66, actual);
            Assert.AreEqual(
                "select g.group_type from phpbb_groups g join phpbb_users u on g.group_id = u.group_id where u.user_id = '" +
                USER_ID + "'",
                command.CommandText);
        }

        [Test]
        public void GetUserGrpupTypeTest_UserIdIsNull()
        {
            Assert.Throws<ArgumentNullException>(() => _provider.GetUserGroupType(null));
        }

        [Test]
        public void GetUserSessionsTest()
        {
            //arrange
            var tunnel = _mockRepository.Stub<SshTunnel>();
            var command = _mockRepository.Stub<DbCommand>();
            var connection = _mockRepository.PartialMock<DbConnection>();

            var table = new DataTable();
            table.Columns.Add("sessonIds");
            table.Rows.Add("expectedId1");
            table.Rows.Add("expectedId2");
            table.Rows.Add("expectedId3");
            using (_mockRepository.Record())
            {
                Expect.Call(_sshTunnelFactory.GetSshTunnel()).Return(tunnel);
                Expect.Call(_factory.CreateConnection()).Return(connection);
                Expect.Call(_builder.ConnectionString).Return(CONNECTION_STRING);
                Expect.Call(tunnel.LocalPort).Return(PORT);
                Expect.Call(connection.ConnectionString).SetPropertyWithArgument(CONNECTION_STRING );
                Expect.Call(connection.CreateCommand()).Return(command);
                Expect.Call(connection.Open);
                Expect.Call(command.ExecuteReader()).Return(table.CreateDataReader());
            }
            //act
            var actual = _provider.GetUserSessions(USER_ID);
            //assert
            Assert.AreEqual(new[] {"expectedId1", "expectedId2", "expectedId3"}, actual);
            Assert.AreEqual(
                "select session_id from fbguildru_forum1.phpbb_sessions where session_user_id = '" + USER_ID + "'",
                command.CommandText);
        }
    }
}